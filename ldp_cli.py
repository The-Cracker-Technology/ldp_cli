#!/opt/ANDRAX/python27/bin/python2.7

import sys
import signal
import threading
import socket
import struct
import time
import cmd

LDP_CLI_VERSION = "v0.2"

LDP_PORT = 646
LDP_VERSION = 1

DEFAULT_HOLD_TIME = 15  #like cisco
DEFAULT_KEEP_ALIVE = 60 #cisco got 180

SO_BINDTODEVICE	= 25

class ldp_msg(object):
    MSG_TYPE_HELLO = 0x0100
    MSG_TYPE_INIT = 0x0200
    MSG_TYPE_KEEPALIVE = 0x0201
    #MSG_TYPE_ADDRESS = 0x0300
    MSG_TYPE_LABEL_MAPPING = 0x0400

    def __init__(self, lsr_id, label_space, msgs):
        self.lsr_id = lsr_id
        self.label_space = label_space
        self.msgs = msgs

    def render(self):
        data = ""
        for x in self.msgs:
            data += x.render()
        return struct.pack("!HH", LDP_VERSION, len(data) + 6) + self.lsr_id + struct.pack("!H", self.label_space) + data;

class ldp_hello_msg(object):
    def __init__(self, id, tlvs):
        self.id = 0
        self.tlvs = tlvs

    def render(self):
        data = ""
        for x in self.tlvs:
            data += x.render()
        return struct.pack("!HHI", ldp_msg.MSG_TYPE_HELLO, len(data) + 4, self.id) + data

class ldp_init_msg(object):
    def __init__(self, id, tlvs):
        self.id = id
        self.tlvs = tlvs

    def render(self):
        data = ""
        for x in self.tlvs:
            data += x.render()
        return struct.pack("!HHI", ldp_msg.MSG_TYPE_INIT, len(data) + 4, self.id) + data

class ldp_keepalive_msg(object):
    def __init__(self, id):
        self.id = id

    def render(self):
        return struct.pack("!HHI", ldp_msg.MSG_TYPE_KEEPALIVE, 4, self.id)

class ldp_label_mapping_msg(object):
    def __init__(self, id, tlvs):
        self.id = id
        self.tlvs = tlvs

    def render(self):
        data = ""
        for x in self.tlvs:
            data += x.render()
        return struct.pack("!HHI", ldp_msg.MSG_TYPE_LABEL_MAPPING, len(data) + 4, self.id) + data

class ldp_tlv(object):
    TLV_TYPE_FORWARDING_EQUIVALENCE_CLASSES = 0x0100
    TLV_TYPE_GENERIC_LABEL = 0x0200
    TLV_TYPE_COMMON_HELLO = 0x0400
    TLV_TYPE_IPV4_TRANSPORT = 0x0401
    TLV_TYPE_COMMON_SESSION = 0x0500
    
    def __init__(self, type):
        self.type = type

    def render(self, data):
        return struct.pack("!HH", self.type, len(data)) + data

class ldp_forwarding_equivalence_classes_tlv(ldp_tlv):
    def __init__(self, fec_elements):
        ldp_tlv.__init__(self, ldp_tlv.TLV_TYPE_FORWARDING_EQUIVALENCE_CLASSES)
        self.fec_elements = fec_elements

    def render(self):
        data = ""
        for x in self.fec_elements:
            data += x.render()
        return ldp_tlv.render(self, data)

class ldp_generic_label_tlv(ldp_tlv):
    def __init__(self, label):
        ldp_tlv.__init__(self, ldp_tlv.TLV_TYPE_GENERIC_LABEL)
        self.label = label

    def render(self):
        return ldp_tlv.render(self, struct.pack("!I", self.label))

class ldp_common_hello_tlv(ldp_tlv):
    def __init__(self, hold_time, targeted = False, request = False):
        ldp_tlv.__init__(self, ldp_tlv.TLV_TYPE_COMMON_HELLO)
        self.hold_time = hold_time
        self.targeted = targeted
        self.request = request

    def render(self):
        data = 0
        if self.targeted:
            data |= 0x6000
        if self.request:
            data |= 0x4000
        return ldp_tlv.render(self, struct.pack("!HH", self.hold_time, data))

class ldp_ipv4_transport_tlv(ldp_tlv):
    def __init__(self, addr):
        ldp_tlv.__init__(self, ldp_tlv.TLV_TYPE_IPV4_TRANSPORT)
        self.addr = addr

    def render(self):
        return ldp_tlv.render(self, socket.inet_aton(self.addr))

class ldp_common_session_tlv(ldp_tlv):
    def __init__(self, keep_alive, rcv_lsr_id, rcv_label_space):
        ldp_tlv.__init__(self, ldp_tlv.TLV_TYPE_COMMON_SESSION)
        self.keep_alive = keep_alive
        self.rcv_lsr_id = rcv_lsr_id
        self.rcv_label_space = rcv_label_space

    def render(self):
        return ldp_tlv.render(self, struct.pack("!HHBBH", LDP_VERSION, self.keep_alive, 0, 0, 0) + self.rcv_lsr_id + struct.pack("!H", self.rcv_label_space))

class ldp_virtual_circuit_fec(object):
    VC_TYPE_ETHERNET = 0x0005
    
    def __init__(self, group_id, vc_id, iface_tlvs, c_bit = False, vc_type = None):
        self.group_id = group_id
        self.vc_id = vc_id
        self.iface_tlvs = iface_tlvs
        self.c_bit = c_bit
        self.vc_type = vc_type

    def render(self):
        data = ""
        for x in self.iface_tlvs:
            data += x.render()
        if not self.vc_type:
            self.vc_type = self.VC_TYPE_ETHERNET
        if self.c_bit:
            self.vc_type &= 0x80
        return struct.pack("!BHBII", 0x80, self.vc_type, len(data) + 4, self.group_id, self.vc_id) + data

class ldp_vc_interface_param_mtu(object):
    VC_INTERFACE_PARAM_MTU = 0x01

    def __init__(self, mtu):
        self.mtu = mtu

    def render(self):
        return struct.pack("!BBH", self.VC_INTERFACE_PARAM_MTU, 4, self.mtu)
    
class ldp_vc_interface_param_vccv(object):
    VC_INTERFACE_PARAM_VCCV = 0x0c

    def __init__(self, cc_type = 0x02, cv_type = 0x02):
        self.cc_type = cc_type
        self.cv_type = cv_type

    def render(self):
        return struct.pack("!BBBB", self.VC_INTERFACE_PARAM_VCCV, 4, self.cc_type, self.cv_type)

class ldp_hello_thread(threading.Thread):
    def __init__(self, addr, interface, hold_time = DEFAULT_HOLD_TIME, targeted = False, request = False, dest = None):
        threading.Thread.__init__(self)
        self.addr = addr
        self.hold_time = hold_time
        self.targeted = targeted
        self.request = request
        self.dest = dest
        self.sock = None
        self.running = True
        self.interface = interface

    def hello(self):
        msg = ldp_msg(socket.inet_aton(self.addr), 0, [ ldp_hello_msg(0, [ ldp_common_hello_tlv(self.hold_time, self.targeted, self.request), ldp_ipv4_transport_tlv(self.addr) ] ) ] )
        data = msg.render()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, SO_BINDTODEVICE, self.interface)
        self.sock.bind(('', LDP_PORT))
        while self.running:
            self.sock.sendto(data, ("224.0.0.2", LDP_PORT))
            for x in interface.peers:
                self.sock.sendto(data, (x, LDP_PORT))
            time.sleep(self.hold_time / 3)

    def run(self):
        self.hello()
        print "Hello thread terminated"
        interface.hello_thread = None

    def quit(self):
        self.running = False
        self.join()

class ldp_listener(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.running = True
        self.listen_sock = None

    def run(self):
        self.listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listen_sock.settimeout(1)
        self.listen_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.listen_sock.bind(("0.0.0.0", LDP_PORT))
        self.listen_sock.listen(1)
        while(self.running):
            try:
                (csock, addr) = self.listen_sock.accept()
            except:
                continue
            csock.settimeout(1)
            interface.add_peer(csock, addr)
        self.listen_sock.close()
        print "Listener terminated"
    
    def quit(self):
        self.running = False
        self.join()

class ldp_peer(threading.Thread):
    def __init__(self, peer, sock, addr, label_space = 0, keep_alive = DEFAULT_KEEP_ALIVE, timeout = 3):
        threading.Thread.__init__(self)
        self.sem = threading.Semaphore()
        self.peer = peer
        self.timeout = timeout
        self.running = True
        self.sock = sock
        self.listen_sock = None
        self.keepalive_msg = None
        self.lsr_id = socket.inet_aton(addr)
        self.id = 1
        self.label_space = label_space
        self.msg = ldp_msg(self.lsr_id, label_space, [ldp_init_msg(self.id, [ldp_common_session_tlv(keep_alive, socket.inet_aton(peer), label_space)])])
        self.keep_alive = keep_alive

    def send(self):
        if not self.sock:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.settimeout(self.timeout)    
            self.sock.connect((self.peer, LDP_PORT))
        
        while(self.running):
            self.sem.acquire()
            try:
                if self.msg:
                    self.sock.send(self.msg.render())
                    self.msg = None
                self.id += 1
                self.keepalive_msg = ldp_msg(self.lsr_id, self.label_space, [ldp_keepalive_msg(self.id)])
                self.sock.send(self.keepalive_msg.render())
            except socket.error:
                self.running = False
            self.sem.release()
            if self.running:
                time.sleep(self.keep_alive / 3)
        self.sock.close()

    def update(self, msg):
        self.sem.acquire()
        self.msg = msg
        self.sem.release()

    def run(self):
        self.send()
        print "LDP peer " + self.peer + " terminated"
        #del interface.peers[self.peer]

    def quit(self):
        self.running = False
        self.join()
    
class ldp_interface(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)
        self.intro = "LDP_CLI " + LDP_CLI_VERSION + " by Daniel Mende - dmende@ernw.de"
        self.prompt = "LDP_CLI> "
        self.hello_thread = None
        self.interface = None
        self.peers = {}
        self.listener = ldp_listener()
        self.ident = None
        self.msg = None

    def do_EOF(self, arg):
        print ""
        self.do_exit("")
    
    def do_exit(self, args):
        print "Bye..."
        if interface.listener:
            interface.listener.quit()
        if interface.hello_thread:
            interface.hello_thread.quit()
        for x in interface.peers:
            interface.peers[x].quit()
        exit(1)

    def help_exit(self):
        print "Quits LDP_CLI"

    def emptyline(self):
        pass

    def default(self, line):       
        try:
            exec(line)
        except Exception, e:
            print e.__class__, ":", e

    def preloop(self):
        cmd.Cmd.preloop(self)
        self._hist = []
        self._locals = {}
        self._globals = {}

    def do_hist(self, args):
        print self._hist

    def help_hist(self):
        print "Prints the command history"

    def precmd(self, line):
        self._hist += [line.strip()]
        return line

    def do_interface(self, args):
        t = args.split()
        self.interface = t[0]

    def help_interface(self):
        print "Set the interface to send ldp hello packets from. ARGS: INTERFACE"

    def do_hello(self, args):
        if self.hello_thread:
            print "Allready sending HELLOs"
        else:
            t = args.split()
            if len(t) == 1:
                self.ident = t[0]
                self.listener.start()
                print "Listener started"
                self.hello_thread = ldp_hello_thread(self.ident, self.interface)
                self.hello_thread.start()
                print "Hello thread started"
            else:
                print "Wrong parameter count"

    def help_hello(self):
        print "Start a simple (untargeted) ldp hello process. ARGS: SOURCEADDR"

    def do_unhello(self, args):
        self.hello_thread.quit()
        self.listener.quit()

    def help_unhello(self):
        print "Stops the ldp hello process."

    def add_peer(self, sock, (addr, port)):
        print "Got new connection from peer " + addr
        self.peers[addr] = ldp_peer(addr, sock, self.ident)
        self.peers[addr].start()

    def do_init(self, args):
        if not self.hello_thread:
            print "You need to start helloing first"
        else:
            t = args.split()
            if len(t) == 1:
                if t[0] not in self.peers:
                    self.peers[t[0]] = ldp_peer(t[0], None, self.ident)
                    self.peers[t[0]].start()
                    print "Peer " + t[0] + " session started"
                else:
                    print "There is allready a peer " + t[0]
            else:
                print "Wrong parameter count"

    def help_init(self):
        print "Initializes a lpd session to the peer for further lable distribution. ARGS: PEER"

    def do_uninit(self, args):
        t = args.split()
        if len(t) == 1:
            if t[0] in self.peers:
                self.peers[t[0]].quit()
                del self.peers[t[0]]
        else:
            print "Wrong parameter count"
        
    def help_uninit(self):
        print "Destroys an initialized ldp session. ARGS: PEER"

    def do_update(self, args):
        t = args.split()
        if len(t) == 1:
            if t[0] in self.peers:
                if self.msg:
                    self.peers[t[0]].update(self.msg)
                    self.msg = None
            else:
                print "Peer " + t[0] + " not initialized"
        else:
            print "Wrong parameter count"

    def help_help(self):
        print """interface br0
hello 10.10.10.100
[ init 192.168.1.1 ]
self.msg = ldp_msg(socket.inet_aton(self.ident), 0, [ ldp_label_mapping_msg(0x2311, [ ldp_forwarding_equivalence_classes_tlv([ ldp_virtual_circuit_fec(0, 300, [ ldp_vc_interface_param_mtu(1500), ldp_vc_interface_param_vccv() ]) ]), ldp_generic_label_tlv(22) ]) ])
update 192.168.1.1
"""

if __name__ == "__main__":
    def sigint(signum, frame):
        if interface.listener:
            interface.listener.quit()
        if interface.hello_thread:
            interface.hello_thread.quit()
        for x in interface.peers:
            interface.peers[x].quit()
        exit(1)
    
    signal.signal(signal.SIGINT, sigint)

    interface = ldp_interface()
    interface.cmdloop()
